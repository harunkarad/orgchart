sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"

], function(Controller, JSONModel) {
	"use strict";

	return Controller.extend("OrgChart.controller.View1", {

		onInit: function() {
			var oModel = new JSONModel(jQuery.sap.getModulePath("OrgChart", "/graph.json"));
			this.getView().setModel(oModel);

			this._oModelSettings = new JSONModel({
				source: "atomicCircle",
				orientation: "LeftRight",
				arrowPosition: "End",
				arrowOrientation: "None",
				nodeSpacing: 40,
				nodePlacement: "Simple",
				mergeEdges: true,
				press: "linePress"
			});
			
			this._sTopSupervisor = 1;

			this.getView().setModel(this._oModelSettings, "settings");

			OrgChart.controller.View1.prototype.linePress = function(oEvent) {
				oEvent.bPreventDefault = true;
			};
			this.initGraph();
			this.getCustomData();
		},

		linePress: function(oEvent) {
			// Prevents render a default popover
			oEvent.preventDefault();
		},

		_getCustomDataValue(obj, key) {
			var customData = obj.getCustomData();
			var keyValue = ""
			$.each(customData, function(i, v) {
				if (v.getKey() == key) {
					keyValue = v.getValue();
				}
			});
			return keyValue;
		},

		getCustomData() {
			debugger;
			var graph = this.byId("graph");

			graph.attachEvent("beforeLayouting", function(oEvent) {
				var nodes = graph.getNodes();
				$.each(nodes, function(i, oNode) {
					if (oNode.getKey() === this._sTopSupervisor) {
						sSupervisor = this._getCustomDataValue(oNode, "supervisor");
						if (sSupervisor) {
							oUpOneLevelButton = new ActionButton({
								title: "Up one level",
								icon: "sap-icon://arrow-top",
								press: function() {
									var aSuperVisors = oNode.getCustomData().filter(function(oData) {
											return oData.getKey() === "supervisor";
										}),
										sSupervisor = aSuperVisors.length > 0 && aSuperVisors[0].getValue();

									this._loadMore(sSupervisor);
									this._sTopSupervisor = sSupervisor;
								}.bind(this)
							});
							oNode.addActionButton(oUpOneLevelButton);
						}
					}
				});
			});
		},

		initGraph: function() {

		OrgChart.controller.View1.prototype.search = function(oEvent) {
				var sKey = oEvent.getParameter("key");

				if (sKey) {
					this._mExplored = [sKey];
					this._sTopSupervisor = sKey;
					this._graph.destroyAllElements();
					this._setFilter();

					oEvent.bPreventDefault = true;
				}
			};

			OrgChart.controller.View1.prototype._setFilter = function() {
				var aNodesCond = [],
					aLinesCond = [];
				var fnAddBossCondition = function(sBoss) {
					aNodesCond.push(new sap.ui.model.Filter({
						path: 'id',
						operator: sap.ui.model.FilterOperator.EQ,
						value1: sBoss
					}));

					aNodesCond.push(new sap.ui.model.Filter({
						path: 'supervisor',
						operator: sap.ui.model.FilterOperator.EQ,
						value1: sBoss
					}));
				};

				var fnAddLineCondition = function(sLine) {
					aLinesCond.push(new sap.ui.model.Filter({
						path: "from",
						operator: sap.ui.model.FilterOperator.EQ,
						value1: sLine
					}));
				};

				this._mExplored.forEach(function(oItem) {
					fnAddBossCondition(oItem);
					fnAddLineCondition(oItem);
				});

				this._graph.getBinding("nodes").filter(new sap.ui.model.Filter({
					filters: aNodesCond,
					and: false
				}));

				this._graph.getBinding("lines").filter(new sap.ui.model.Filter({
					filters: aLinesCond,
					and: false
				}));
			};

			OrgChart.controller.View1.prototype._openDetail = function(oNode, oButton) {
				var sTeamSize = this._getCustomDataValue(oNode, "team");

				if (!this._oQuickView) {
					this._oQuickView = sap.ui.xmlfragment("sap.suite.ui.commons.sample.NetworkGraphOrgChart.TooltipFragment", this);
				}

				this._oQuickView.setModel(new JSONModel({
					icon: oNode.getImage() && oNode.getImage().getProperty("src"),
					title: oNode.getDescription(),
					description: this._getCustomDataValue(oNode, "position"),
					location: this._getCustomDataValue(oNode, "location"),
					showTeam: !!sTeamSize,
					teamSize: sTeamSize,
					email: this._getCustomDataValue(oNode, "email"),
					phone: this._getCustomDataValue(oNode, "phone")
				}));

				jQuery.sap.delayedCall(0, this, function() {
					this._oQuickView.openBy(oButton);
				});
			};

			OrgChart.controller.View1.prototype.suggest = function(oEvent) {
				var aSuggestionItems = [],
					aItems = this._oModel.getData().nodes,
					aFilteredItems = [],
					sTerm = oEvent.getParameter("term");

				sTerm = sTerm ? sTerm : "";

				aFilteredItems = aItems.filter(function(oItem) {
					var sTitle = oItem.title ? oItem.title : "";
					return sTitle.toLowerCase().indexOf(sTerm.toLowerCase()) !== -1;
				});

				aFilteredItems.sort(function(oItem1, oItem2) {
					var sTitle = oItem1.title ? oItem1.title : "";
					return sTitle.localeCompare(oItem2.title);
				}).forEach(function(oItem) {
					aSuggestionItems.push(new sap.m.SuggestionItem({
						key: oItem.id,
						text: oItem.title
					}));
				});

				this._graph.setSearchSuggestionItems(aSuggestionItems);
				oEvent.bPreventDefault = true;
			};
		}
	});

});